package com.example.rooxdemo;

import com.rooxteam.uidm.sdk.spring.configuration.OtpPerOperationApiConfiguration;
import com.rooxteam.uidm.sdk.spring.policy.PermissionsEvaluationServiceConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({
		OtpPerOperationApiConfiguration.class,
		PermissionsEvaluationServiceConfiguration.class
})
public class RooxdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RooxdemoApplication.class, args);
	}

}
