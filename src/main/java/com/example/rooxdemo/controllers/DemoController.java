package com.example.rooxdemo.controllers;

import com.rooxteam.sso.aal.Principal;
import com.rooxteam.uidm.sdk.spring.authentication.AuthenticationState;
import com.rooxteam.uidm.sdk.spring.policy.PermissionsEvaluationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.rooxteam.uidm.sdk.spring.authentication.AalAuthorizationClient.EVALUATION_ADVICES_ATTRIBUTE_NAME;
import static com.rooxteam.uidm.sdk.spring.authentication.AalAuthorizationClient.EVALUATION_CLAIMS_ATTRIBUTE_NAME;

/**
 * @author sergey.syroezhkin
 * @since 19.10.2020
 */
@RestController
@RequestMapping("/api")
public class DemoController {

    @Autowired
    private PermissionsEvaluationService permissionsEvaluationService;

    @RequestMapping(method = RequestMethod.GET, value = "/hello2")
    public String getHello2() {
        return "Hello2 Roox!";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/hello")
    @PreAuthorize("isAuthenticated() && @uidmAuthz.isAllowed('/hello','GET')")
    public String getHello() {
        return "Hello Roox!";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/demo0")
    @PreAuthorize("isAuthenticated()")
    public String getDemo0() {
        return "Demo Roox0!";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/demo")
    @PreAuthorize("isAuthenticated() && @uidmAuthz.isAllowed('/demo','GET')")
    public String getDemo() {
        return "Demo Roox!";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/demo2")
    @PreAuthorize("isAuthenticated() && isResourceAllowed('/demo','GET')")
    public String getDemo2() {
        return "Demo Roox 2!";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/demo3")
    @PreAuthorize("@uidmAuthz.isAllowed('/demo','GET') || @uidmAuthz.isAllowed('/hello','GET')")
    public String getDemo3() {
        return "Demo Roox 3!";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/demo4")
    @PreAuthorize("@uidmAuthz.isAllowed('/demo','GET') && @uidmAuthz.isAllowed('/hello','GET')")
    public String getDemo4() {
        return "Demo Roox 4!";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/upload")
    @PreAuthorize("isAuthenticated() && @uidmAuthz.isAllowed('/upload','POST')")
    public String uploadGet() {
        return "Upload allowed";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/upload")
    @PreAuthorize("isAuthenticated() && (@uidmAuthz.isAllowed('/upload','POST') || @uidmAuthz.isAllowed('/upload2','POST'))")
    public String uploadPost(@RequestBody Object body) {
        return "Upload allowed";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sign")
    @PreAuthorize("isAuthenticated() && (@uidmAuthz.isAllowed('/payments/:id/sign','POST'))")
    public String signDocuments(@RequestBody Object body) {
        return "Document signed";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/policy")
    @ResponseBody
    public Object policy(Authentication authentication) {
        Principal aalPrincipal = null;
        if (authentication instanceof AuthenticationState) {
            aalPrincipal = (Principal) ((AuthenticationState)authentication).getAttributes().get("aalPrincipal");
        }
        Map<String, Set<RequestMethod>> policy = permissionsEvaluationService.evaluate(authentication, aalPrincipal);
        return policy;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/payment")
    @PreAuthorize("isAuthenticated() && (@uidmAuthz.isAllowed('/payments/:id/sign','POST', {'body':#body}))")
    public Object payment(@RequestBody Object body, AuthenticationState authenticationState) {
        Map<String, Object> response = new HashMap<>();
        response.put("body", body);
        Optional.ofNullable(authenticationState)
                .map(AuthenticationState::getAttributes)
                .map(attrs -> attrs.get(EVALUATION_CLAIMS_ATTRIBUTE_NAME))
                .ifPresent(v -> response.put("claims", v));
        return response;
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleAccessDeniedException(AuthenticationState authenticationState, AccessDeniedException e) {
        Map<String, Object> response = new HashMap<>();
        response.put("error", e.getMessage());
        Optional.ofNullable(authenticationState)
                .map(AuthenticationState::getAttributes)
                .map(attrs -> attrs.get(EVALUATION_ADVICES_ATTRIBUTE_NAME))
                .ifPresent(v -> response.put("advices", v));
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
    }

}
